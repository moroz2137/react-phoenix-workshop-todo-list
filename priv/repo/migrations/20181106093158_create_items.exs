defmodule Nigiyaka.Repo.Migrations.CreateItems do
  use Ecto.Migration

  def change do
    create table(:items) do
      add :title, :string
      add :done_at, :naive_datetime

      timestamps()
    end

  end
end
