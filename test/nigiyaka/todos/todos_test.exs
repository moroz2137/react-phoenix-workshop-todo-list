defmodule Nigiyaka.TodosTest do
  use Nigiyaka.DataCase

  alias Nigiyaka.Todos

  describe "items" do
    alias Nigiyaka.Todos.Item

    @valid_attrs %{done_at: ~N[2010-04-17 14:00:00], title: "some title"}
    @update_attrs %{done_at: ~N[2011-05-18 15:01:01], title: "some updated title"}
    @invalid_attrs %{done_at: nil, title: nil}

    def item_fixture(attrs \\ %{}) do
      {:ok, item} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Todos.create_item()

      item
    end

    test "list_items/0 returns all items" do
      item = item_fixture()
      assert Todos.list_items() == [item]
    end

    test "get_item!/1 returns the item with given id" do
      item = item_fixture()
      assert Todos.get_item!(item.id) == item
    end

    test "create_item/1 with valid data creates a item" do
      assert {:ok, %Item{} = item} = Todos.create_item(@valid_attrs)
      assert item.done_at == ~N[2010-04-17 14:00:00]
      assert item.title == "some title"
    end

    test "create_item/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Todos.create_item(@invalid_attrs)
    end

    test "update_item/2 with valid data updates the item" do
      item = item_fixture()
      assert {:ok, %Item{} = item} = Todos.update_item(item, @update_attrs)

      assert item.done_at == ~N[2011-05-18 15:01:01]
      assert item.title == "some updated title"
    end

    test "update_item/2 with invalid data returns error changeset" do
      item = item_fixture()
      assert {:error, %Ecto.Changeset{}} = Todos.update_item(item, @invalid_attrs)
      assert item == Todos.get_item!(item.id)
    end

    test "delete_item/1 deletes the item" do
      item = item_fixture()
      assert {:ok, %Item{}} = Todos.delete_item(item)
      assert_raise Ecto.NoResultsError, fn -> Todos.get_item!(item.id) end
    end

    test "change_item/1 returns a item changeset" do
      item = item_fixture()
      assert %Ecto.Changeset{} = Todos.change_item(item)
    end

    test "toggle_item/1 sets done_at when done_at is nil" do
      item = item_fixture(%{done_at: nil})
      refute item.done_at
      {:ok, toggled} = Todos.toggle_item(item)
      assert toggled.done_at
    end

    test "toggle_item/1 clears done_at when not nil" do
      item = item_fixture(%{done_at: DateTime.utc_now()})
      assert item.done_at
      {:ok, toggled} = Todos.toggle_item(item)
      refute toggled.done_at
    end
  end
end
