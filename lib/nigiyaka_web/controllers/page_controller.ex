defmodule NigiyakaWeb.PageController do
  use NigiyakaWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
