defmodule NigiyakaWeb.Router do
  use NigiyakaWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", NigiyakaWeb do
    pipe_through :api

    resources "/items", ItemController, except: [:new, :edit]
    patch("/items/:id/toggle", ItemController, :toggle)
  end

  scope "/", NigiyakaWeb do
    pipe_through :browser

    get "/", PageController, :index
  end
end
