defmodule Nigiyaka.Repo do
  use Ecto.Repo,
    otp_app: :nigiyaka,
    adapter: Ecto.Adapters.Postgres
end
