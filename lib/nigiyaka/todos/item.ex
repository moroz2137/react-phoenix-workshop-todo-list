defmodule Nigiyaka.Todos.Item do
  use Ecto.Schema
  import Ecto.Changeset

  schema "items" do
    field :done_at, :naive_datetime
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(item, attrs) do
    item
    |> cast(attrs, [:title, :done_at])
    |> validate_required([:title])
  end
end
