import css from "../css/app.sass";
import React from 'react';
import ReactDOM from 'react-dom';
import TodoApp from './TodoApp';

const todoContainer = document.getElementById('todo-app');
todoContainer && ReactDOM.render(<TodoApp />, todoContainer);