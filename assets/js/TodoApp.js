import React, { Fragment } from 'react';
import TodoList from "./components/TodoList";
import NewTodoForm from "./components/NewTodoForm";
import axios from 'axios';

const Header = ({ children }) => <h1 className="todo-header">{children}</h1>;

export default class TodoApp extends React.Component {
  state = {
    items: [],
    title: ''
  }

  componentDidMount() {
    this.fetchTodos();
  }

  fetchTodos = async () => {
    const response = await axios.get('/api/items');
    this.setState({ items: response.data.data });
  }

  addNewItem = async (title) => {
    const response = await axios.post('/api/items', { item: { title } });
    const items = [...this.state.items, response.data.data];
    this.setState({ items });
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleFormSubmit = (e) => {
    e.preventDefault();

    const { title } = this.state;
    if (!title) return;
    this.addNewItem(title);
    this.setState({ title: '' });
  }

  toggleItem = async (id) => {
    await axios.patch(`/api/items/${id}/toggle`);
    this.setState({
      items: this.state.items.map(item => {
        return (item.id === id) ?
          { ...item, done: !item.done } :
          item;
      })
    });
  }

  render() {
    const { items, title } = this.state;

    return (
      <Fragment>
        <Header>今日待辦事項</Header>
        <NewTodoForm
          handleFormSubmit={this.handleFormSubmit}
          onChange={this.onChange}
          title={title}
        />
        <TodoList
          items={items}
          toggleItem={this.toggleItem}
        />
      </Fragment>
    )
  }
}
