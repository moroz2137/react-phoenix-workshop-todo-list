import React from 'react';

export default class NewTodoForm extends React.Component {
  render() {
    const { title, handleFormSubmit, onChange } = this.props;
    return (
      <form
        className="new-todo-form"
        onSubmit={handleFormSubmit}
      >
        <input
          type="text"
          autoComplete="off"
          name="title"
          placeholder="有何貴幹？"
          onChange={onChange}
          value={title}
        />
        <button type="submit">追加</button>
      </form>
    )
  }
}
