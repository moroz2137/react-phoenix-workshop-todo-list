import React from 'react';

const TodoItem = ({ title, done, handleClick }) => {
  const className = done ? 'is-done' : '';
  return (
    <li className={className} onClick={handleClick}>
      {title}
    </li>
  )
}

export default class TodoList extends React.Component {
  render() {
    const items = this.props.items.sort((a, b) => b.id - a.id);
    return <ul className="todo-list">
      {items.map(item => (
        <TodoItem
          key={`item-${item.id}`}
          handleClick={() => this.props.toggleItem(item.id)}
          {...item}
        />
      ))}
    </ul>
  }
}
